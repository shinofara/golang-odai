module golang-odai

go 1.12

require (
	contrib.go.opencensus.io/exporter/jaeger v0.2.0
	github.com/apache/thrift v0.13.0 // indirect
	github.com/go-chi/chi v4.0.2+incompatible
	github.com/go-sql-driver/mysql v1.4.1
	github.com/golang/groupcache v0.0.0-20191027212112-611e8accdfc9 // indirect
	github.com/google/wire v0.3.0
	github.com/gorilla/sessions v1.2.0
	github.com/hashicorp/golang-lru v0.5.3 // indirect
	github.com/jinzhu/configor v1.1.1
	github.com/jinzhu/gorm v1.9.11
	github.com/kr/pretty v0.1.0 // indirect
	github.com/pkg/errors v0.8.1
	github.com/uber/jaeger-client-go v2.20.1+incompatible // indirect
	github.com/unrolled/render v1.0.1
	go.opencensus.io v0.22.2
	golang.org/x/crypto v0.0.0-20190701094942-4def268fd1a4
	golang.org/x/sync v0.0.0-20190911185100-cd5d95a43a6e // indirect
	google.golang.org/api v0.13.0 // indirect
	google.golang.org/appengine v1.6.5 // indirect
	gopkg.in/yaml.v2 v2.2.5 // indirect
)

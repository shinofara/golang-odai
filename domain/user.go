package domain

type User struct {
	ID               uint32
	Name             string
	AuthenticationID string
}

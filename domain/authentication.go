package domain

type Authentication struct {
	ID       string
	Email    string
	Password string
}
